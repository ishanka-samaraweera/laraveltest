  $(document).ready(function () {
    $('#product').click(function(e){
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      var data = new FormData();
      data.append('productname', $('#productname').val());
      data.append('quantity', $('#quantity').val());
      data.append('price', $('#price').val());
      $.ajax({
        url: "/product",
        type: "POST",
        data: data,
        processData: false,
        contentType: false,
        success: function(data){
          data = $.parseJSON(data);
          $(".print-error-msg").addClass('hidden');
          $(".alert-success").removeClass('hidden');
          window.location.reload('/product');
        },
        error: function(xhr, status, error) {
          var err = eval("(" + xhr.responseText + ")");
          $(".print-error-msg").find("ul").html('');
          $(".alert-success").addClass('hidden');
    			$(".print-error-msg").removeClass('hidden');
    			$.each( err.errors, function( key, value ) {
    				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    			});
        }
      });
    });

    $('.button_update').click(function(e){
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      var data = new FormData();
      var id = $(this).attr('id');
      var total_without_current = $('#'+id+'_total_without_current').val();
      data.append('id', $('.id').val());
      data.append('productname', $('#'+id+'_productname_update').text());
      data.append('quantity', $('#'+id+'_quantity_update').text());
      data.append('price', $('#'+id+'_price_update').text());
      $.ajax({
        url: "/product/"+ id,
        type: "POST",
        data: data,
        processData: false,
        contentType: false,
        success: function(data){
          data = $.parseJSON(data);
          var final_total = parseFloat(total_without_current) + parseFloat(data.quantity) * parseFloat(data.price);
          $('#'+id+'_total_number').text(data.quantity * data.price);
          $('#final_total').text(final_total);
          $(".print-error-msg").addClass('hidden');
          $(".alert-success").removeClass('hidden');
        },
        error: function(xhr, status, error) {
          var err = eval("(" + xhr.responseText + ")");
          $(".print-error-msg").find("ul").html('');
          $(".alert-success").addClass('hidden');
    			$(".print-error-msg").removeClass('hidden');
    			$.each( err.errors, function( key, value ) {
    				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    			});
        }
      });
    });

    $('.editable').mouseover(function(){
      $(this).addClass('clickable-row');
    });
    $('.editable').mouseout(function(){
      $(this).removeClass('clickable-row');
    });
  });
