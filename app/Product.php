<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \Okipa\LaravelModelJsonStorage\ModelJsonStorage;

    protected $fillable = array('productname', 'quantity', 'price');

}
