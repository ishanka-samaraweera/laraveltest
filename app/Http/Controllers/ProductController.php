<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    /**
    * View product page
    *
    *
    * return view
    **/
    public function index(){

      $products = app(Product::class)->all();
      $total_price = 0;
      if(!empty($products)){
        foreach ($products as $product) {
          $total_price += $product->quantity * $product->price;
        }
      }

      return view('product')->with('products', $products)->with('total_price', $total_price);
    }

    /**
    * Store product in the json file
    * @param Request $request
    *
    * return view
    **/
    public function store(Request $request){

      $validate = $this->productDataValidator($request->all())->validate();

      if ($validate && $validate->fails())
      {
        return response(
          array(
          'success' => false,
          'errors' => $validate->errors()->toArray()
          )
        );
      }
      else{
        $product = app(Product::class)->create([
          'productname' => $request->productname,
          'quantity' => $request->quantity,
          'price' => $request->price
        ]);

        echo json_encode($request->all());
      }
    }

    /**
    * Store product in the json file
    * @param Request $request
    * @param int $id
    * return view
    **/
    public function update(Request $request, $id){
      $validate = $this->productDataValidator($request->all())->validate();

      if ($validate && $validate->fails())
      {
        return response(
          array(
          'success' => false,
          'errors' => $validate->errors()->toArray()
          )
        );
      }
      else{
        $product = app(Product::class)->where('id', $id)->first();

        $product->update([
          'id' => $request->id,
          'productname' => $request->productname,
          'quantity' => $request->quantity,
          'price' => $request->price
        ]);

        echo json_encode($request->all());
      }
    }

    /**
     * Get a validator for an incoming feedback request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function productDataValidator(array $data)
    {
        return Validator::make($data, [
            'productname' => 'required',
            'quantity' => 'required|integer',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/'
        ]);
    }
}
