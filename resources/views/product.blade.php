@extends('app')

@section('content')

<div class="alert alert-success alert-block hidden">
  <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ trans('messages.success_product_add') }}</strong>
</div>
<div class="alert alert-danger print-error-msg hidden">
  <ul></ul>
</div>
<div class="content">
  <form>
    <div class="form-group">
      <label for="productName">Product Name</label>
      <input type="text" class="form-control" id="productname" placeholder="Product Name">
    </div>
    <div class="form-group">
      <label for="quantity">Quantity in stock</label>
      <input type="text" class="form-control" id="quantity" placeholder="Quantity in stock">
    </div>
    <div class="form-group">
      <label for="price">Price per item</label>
      <input type="text" class="form-control" id="price" placeholder="Price per item">
    </div>
    <button class="btn btn-primary" id="product">Submit</button>
  </form>

  <div class="card">
    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Products</h3>
    <div class="card-body">
      <div id="producttable" class="table-editable">
        <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i class="fas fa-plus fa-2x"
              aria-hidden="true"></i></a></span>
        <table class="table table-bordered table-responsive-md table-striped text-center">
          <tr>
            <th class="text-center">Product Name</th>
            <th class="text-center">Quantity of Stock</th>
            <th class="text-center">Price per item</th>
            <th class="text-center">Date time submitted</th>
            <th class="text-center">Total value number</th>
            <th class="text-center"></th>
          </tr>
          @if(!empty($products))
            @foreach($products as $product)
            <tr>
              <form>
                <input type="hidden" value="{{ $total_price - ($product->quantity * $product->price) }}" id="{{ $product->id }}_total_without_current">
                <td class="pt-3-half editable" contenteditable="true" id="{{ $product->id }}_productname_update" value="{{ $product->productname }}">{{ $product->productname }}</td>
                <td class="pt-3-half editable" contenteditable="true" id="{{ $product->id }}_quantity_update">{{ $product->quantity }}</td>
                <td class="pt-3-half editable" contenteditable="true" id="{{ $product->id }}_price_update">{{ $product->price }}</td>
                <td class="pt-3-half">{{ $product->created_at }}</td>
                <td class="pt-3-half" id="{{ $product->id }}_total_number">{{ $product->quantity * $product->price }}</td>
                <td>
                  <span class="table_update"><button type="button" class="button_update" id="{{ $product->id }}" class="btn btn-rounded btn-sm my-0" title="To update, click on the table cells">Update</button></span>
                </td>
              </form>
            </tr>
            @endforeach
          @endif
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td id="final_total">{{ $total_price }}</td>
              <td></td>
          </tr>

        </table>
      </div>
    </div>
  </div>
</div>


@endsection
